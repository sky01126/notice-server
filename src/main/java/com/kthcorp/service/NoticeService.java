/*
 * Copyright ⓒ [2018] KTH corp.All rights reserved.
 *
 * This is a proprietary software of KTH corp, and you may not use this file except in
 * compliance with license with license agreement with KTH corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of KTH corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 */

package com.kthcorp.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * NoticeService
 *
 * @author <a href="mailto:ky.son@kt.com"><b>손근양</b></a>
 * @version 1.0.0
 * @since 7.0
 */
@Service
public class NoticeService {

	private static final Logger log = LoggerFactory.getLogger(NoticeService.class);

	private static final String PREFIX = "pmfile_";

	@Value("${notice.pm.path}")
	private String noticeFilePath;

	@Value("${notice.pm.default-file}")
	private String noticeDefaultFile;

	@Value("${notice.pm.contents}")
	private String noticeContents;

	@Cacheable(cacheNames = "default-cache", key = "'get-notice'", unless = "#result == null || #result.isEmpty()")
	public String getNotice() {
		// 오늘 날짜로 등록된 공지가 있는지 확인한다.
		File path = new File(noticeFilePath);
		if (path.exists() && path.isDirectory()) {
			String prefix = PREFIX + "" + DateTime.now().toString("yyyyMMdd");
			File[] noticeFiles = path.listFiles(new NoticeFilenameFilter(prefix));
			if (noticeFiles != null && noticeFiles.length > 0) {
				Arrays.sort(noticeFiles, new NoticeFilenameDescCompare());
				for (File f : noticeFiles) {
					return read(f);
				}
			}
		}
		return getDefaultNotice();
	}

	/**
	 * 기본 공지사항을 가져온다.
	 *
	 * @return 공지사항.
	 */
	private String getDefaultNotice() {
		File defaultFile = new File(noticeDefaultFile);
		if (!defaultFile.exists() || !defaultFile.isFile()) {
			return noticeContents;
		}
		return read(defaultFile);
	}

	/**
	 * 파일 정보를 읽어온다.
	 *
	 * @param file 파일ㄹ 경로.
	 * @return 파일 내용.
	 */
	private String read(File file) {
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))) {
			log.debug("\"{}\" 공지 파일 READ...", file.getName());
			String str;
			String result = "";
			while (StringUtils.isNotBlank((str = reader.readLine()))) {
				// result += StringUtils.trim(str) + " ";
				result += str + "\n";
			}
			return StringUtils.trim(result);
		} catch (IOException e) {
			throw new IllegalStateException("DATA file read fail", e);
		}
	}

}

/**
 * 날짜 순으로 내림차순.
 *
 * @author <a href="mailto:ky.son@kt.com"><b>손근양</b></a>
 * @version 1.0.0
 * @linkplain JDK 1.7
 * @see Comparator
 * @since 2015. 1. 22.
 */
class NoticeFilenameDescCompare implements Comparator<File> {

	/**
	 * 날짜 순으로 내림차순(DESC)
	 */
	@Override
	public int compare(File arg0, File arg1) {
		// return new Long((arg0).lastModified()).compareTo(new Long((arg1).lastModified()));
		return new Long((arg1).lastModified()).compareTo(new Long((arg0).lastModified()));

	}

}

/**
 * 공지사항 파일 필터.
 *
 * @version 1.0.0
 * @see FilenameFilter
 * @since 7.0
 */
class NoticeFilenameFilter implements FilenameFilter {

	/**
	 * 파일 Prefix
	 */
	private final String prefix;

	/**
	 * @param prefix 데이터 파일 Prefix
	 */
	public NoticeFilenameFilter(String prefix) {
		this.prefix = prefix;
	}

	@Override
	public boolean accept(File dir, String name) {
		File file = new File(dir, name);
		return (file.isFile() && StringUtils.startsWith(name, prefix)) ? true : false;
	}

}
