/*
 * Copyright(C) 2017 KT Hitel Co., Ltd. all rights reserved.
 *
 * This is a proprietary software of KTH corp, and you may not use this file except in
 * compliance with license with license agreement with KTH corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of KTH corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 */

package com.kthcorp;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.resource.PathResourceResolver;
import org.springframework.web.servlet.resource.WebJarsResourceResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 * Web Mvc Configuration.
 *
 * @author <a href="mailto:ky.son@kt.com"><b>손근양</b></a>
 * @version 1.0.0
 * @see WebMvcConfigurerAdapter
 * @since 7.0
 */
@Configuration
@EnableWebMvc
public class WebMvcConfiguration extends WebMvcConfigurerAdapter {

	/**
	 * Add handlers to serve static resources such as images, js, and, css
	 * files from specific locations under web application root, the classpath,
	 * and others.
	 *
	 * @param registry ResourceHandlerRegistry
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		// Static Resource 등록.
		registry.addResourceHandler("/static/**") // Resource Handler
				.addResourceLocations("classpath:/static/") // Resource Locations
				// .setCachePeriod(31556926); // Cache 기간 설정
				.resourceChain(true) // Resource Chain True
				.addResolver(new PathResourceResolver());

		// webjars를 Resource 등록.
		// webjars의 정확한 버전을 몰라도 사용할 수 있도록한다. ex. /webjars/jquery/jquery.min.js
		registry.addResourceHandler("/webjars/**") // Resource Handler
				.addResourceLocations("classpath:/META-INF/resources/webjars/") // Resource Locations
				// .setCachePeriod(31556926) // Cache 기간 설정
				.resourceChain(true) // Resource Chain True
				.addResolver(new WebJarsResourceResolver());

		// Cross Domain 등록.
		registry.addResourceHandler("/crossdomain.xml") // Resource Handler
				.addResourceLocations("/crossdomain.xml"); // Resource Locations
		// .setCachePeriod(31556926); // Cache 기간 설정
	}

	/**
	 * Configure View resolver to provide HTML output This is the default format in absence of any type suffix.
	 *
	 * @return ViewResolver
	 */
	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	/**
	 * favicon 404 에러 처리.
	 *
	 * @return
	 */
	@Bean
	public WebMvcConfigurerAdapter faviconWebMvcConfiguration() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addResourceHandlers(ResourceHandlerRegistry registry) {
				registry.setOrder(Integer.MIN_VALUE);
				registry.addResourceHandler("/favicon.ico").addResourceLocations("/").setCachePeriod(0);
			}
		};
	}

}
