package com.kthcorp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	// ------------------------------- YAML Config
	/**
	 * YAML Property Resource Read.
	 *
	 * @return Property SourcesPlaceholder Configurer
	 */
	@Bean
	public PropertySourcesPlaceholderConfigurer properties() {
		List<Resource> resources = new ArrayList<>();
		resources.add(new ClassPathResource("notice.yml"));
		PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
		YamlPropertiesFactoryBean factory = new YamlPropertiesFactoryBean();
		factory.setResources(resources.toArray(new Resource[resources.size()]));
		configurer.setProperties(factory.getObject());
		return configurer;
	}

}
