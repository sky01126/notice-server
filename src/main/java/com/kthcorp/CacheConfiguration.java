/*
 * Copyright(C) 2017 KT Hitel Co., Ltd. all rights reserved.
 *
 * This is a proprietary software of KTH corp, and you may not use this file except in
 * compliance with license with license agreement with KTH corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of KTH corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 */

package com.kthcorp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Cache Configuration
 *
 * @author <a href="mailto:ky.son@kt.com"><b>손근양</b></a>
 * @version 1.0.0
 * @see CachingConfigurerSupport
 * @since 7.0
 */
@Configuration
@EnableCaching
@SuppressWarnings("all")
public class CacheConfiguration extends CachingConfigurerSupport {

	private static final Logger log = LoggerFactory.getLogger(CacheConfiguration.class);

	@Bean
	@Override
	public CacheManager cacheManager() {
		return new EhCacheCacheManager(ehCacheManager());
	}

	@Bean(destroyMethod = "shutdown")
	public net.sf.ehcache.CacheManager ehCacheManager() {
		net.sf.ehcache.config.Configuration config = new net.sf.ehcache.config.Configuration();
		config.addCache(getDefaultCache());
		return net.sf.ehcache.CacheManager.newInstance(config);
	}

	/**
	 * 기본적으로 사용될 Cache
	 *
	 * @return net.sf.ehcache.config.CacheConfiguration
	 */
	private net.sf.ehcache.config.CacheConfiguration getDefaultCache() {
		net.sf.ehcache.config.CacheConfiguration cache = new net.sf.ehcache.config.CacheConfiguration();
		cache.setName("default-cache");

		// 캐싱 데이터를 영원히 유지할 것인지 여부
		// true이면 timeout 관련 설정은 무시되고, Element가 캐시에서 삭제되지 않는다.
		cache.setEternal(false);

		// 메모리에 생성될 객체의 최대 수를 설정 (0 = no limit)
		cache.setMaxEntriesLocalHeap(0);

		// Element가 지정한 시간 동안 사용(조회)되지 않으면 캐시에서 제거된다.
		// 이 값이 0인 경우 조회 관련 만료 시간을 지정하지 않는다. 기본값은 0이다.
		cache.setTimeToIdleSeconds(24 * 60 * 60); // 1일동안 Cache 유지.

		// Element가 존재하는 시간. 이 시간이 지나면 캐시에서 제거된다.
		// 이 시간이 0이면 만료 시간을 지정하지 않는다. 기본값은 0이다.
		cache.setTimeToLiveSeconds(24 * 60 * 60); // 1일동안 Cache 유지.

		// 저장 공간이 꽉 찰 경우 데이터를 제거하는 규칙 지정
		// LRU - 데이터의 접근 시점을 기준으로 최근 접근 시점이 오래된 데이터부터 삭제
		// LFU - 데이터의 이용 빈도 수를 기준으로 이용 빈도가 가장 낮은 것부터 삭제
		// FIFO - 먼저 저장된 데이터를 우선 삭제
		cache.setMemoryStoreEvictionPolicy("LRU");
		return cache;
	}

}
