/*
 * Copyright ⓒ [2017] KTH corp.All rights reserved.
 *
 * This is a proprietary software of KTH corp, and you may not use this file except in
 * compliance with license with license agreement with KTH corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of KTH corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 */

package com.kthcorp.web.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.kthcorp.service.NoticeService;

/**
 * NoticeController.java
 *
 * @author <a href="mailto:ky.son@kt.com"><b>손근양</b></a>
 * @version 1.0.0
 * @see
 * @since 7.0
 */
@RestController
public class NoticeController {

	@Autowired
	private NoticeService noticeService;

	@RequestMapping(path = "/**")
	public String notice(HttpServletRequest request, HttpServletResponse response) {
		setJsonContentType(request, response);
		return noticeService.getNotice();
	}

	@RequestMapping(path = "/web/v100/notice")
	public ModelAndView webNotice(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("webview");
		return mav;
	}

	/**
	 * Cahce된 모든 데이터를 삭제한다.
	 *
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @return ResponseEntity
	 */
	@RequestMapping(path = "/cache/clear")
	@CacheEvict(cacheNames = "default-cache", allEntries = true)
	public ResponseEntity<Object> cacheClear(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> map = new HashMap<>();
		map.put("res_code", 200);
		map.put("res_msg", "Cache Clear 성공");
		return new ResponseEntity<Object>(map, HttpStatus.OK);
	}

	/**
	 * JSON의 ContextType을 설정한다.
	 *
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 */
	public void setJsonContentType(final HttpServletRequest request, final HttpServletResponse response) {
		if (isIE(request)) {
			response.setContentType(MediaType.TEXT_PLAIN_VALUE);
		} else {
			response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
		}
	}

	/**
	 * 브라우저가 Internet Explorer 인지 체크한다.
	 *
	 * @param request HttpServletRequest
	 * @return true / false
	 */
	public static final boolean isIE(final HttpServletRequest request) {
		String userAgent = request.getHeader("User-Agent");
		if (StringUtils.isBlank(userAgent)) {
			return false;
		} else if (StringUtils.containsIgnoreCase(userAgent, "MSIE")
				|| StringUtils.containsIgnoreCase(userAgent, "Trident")) {
			return true;
		}
		return false;
	}

}
